from django.shortcuts import render, redirect
from .forms import MatkulForm
from .models import Matkul

# Create your views here.
def s(request):
    return render(request, 'story4/s.html')

def homes(request):
    return render(request, 'story4/homes.html')

def skills(request):
    return render(request, 'story4/skills.html')

def experiences(request):
    return render(request, 'story4/experiences.html')

def achievements(request):
    return render(request, 'story4/achievements.html')

def matkul_create(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/jadwal')
    context = {
        'form' : form
    }
    return render(request, 'story4/form.html',context)

def jadwalmatkul(request):
    posts = Matkul.objects.all()
    context = {
        'jadwal' : posts
    }
    return render(request, 'story4/jadwalmatkul.html', context)

def delete_matkul(request, delete_subject):
    Matkul.objects.filter(subject=delete_subject).delete()
    return redirect('/jadwal')

def detail(request, detail_subject):
    subjek = Matkul.objects.get(subject=detail_subject)
    context = {
        'subjek' : subjek
    }
    return render(request, 'story4/detail.html', context)