from django.db import models

# Create your models here.
class Matkul(models.Model):
    LIST_TERM=(
        ('2019/2020-1','2019/2020-1'),
        ('2019/2020-2','2019/2020-2'),
        ('2020/2021-3','2020/2021-3'),
        ('2020/2021-4','2020/2021-4'),
        ('2021/2022-5','2021/2022-5'),
        ('2021/2022-6','2021/2022-6'),
        ('2022/2023-7','2022/2023-7'),
        ('2022/2023-8','2022/2023-8')
    )
    term        = models.CharField(
        max_length=120,#max_length = required
        choices=LIST_TERM,
        default='2020/2021-3'
        ) 
    subject     = models.CharField(max_length=120) #max_length = required
    LIST_CREDITS=(
        ('1','1'),
        ('2','2'),
        ('3','3'),
        ('4','4'),
        ('6','6')
    )
    credits    = models.CharField(
        max_length=120,#max_length = required
        choices=LIST_CREDITS,
        default='1'
        ) 
    description = models.TextField(max_length= 500, default ="")
    lecturer    = models.CharField(max_length = 120) #max_length = required
    classroom   = models.CharField(max_length=120) #max_length = required
    featured    = models.BooleanField(default = True)

    def __str__(self):
        return"self.subject"
