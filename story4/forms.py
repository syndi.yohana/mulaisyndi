from django import forms
from .models import Matkul
from django.forms import ModelForm

class MatkulForm(forms.ModelForm):
    class Meta :
        model = Matkul
        fields = [
            'term',
            'subject',
            'credits',
            'description',
            'lecturer',
            'classroom',           
        ]

        widgets = {
            'term':forms.Select(attrs={'class':'form-control','placeholder':'Select your term'}),
            'subject':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your subject'}),
            'credits':forms.Select(attrs={'class':'form-control','placeholder':'Select your credits'}),
            'description':forms.Textarea(attrs={'class':'form-control','placeholder':'Please describe your subject!'}),
            'lecturer':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your Lecturer'}),
            'classroom':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your Classroom'})
        }