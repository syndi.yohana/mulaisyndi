"""mulaisyndi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.homes, name="homes"),
    path('s/', views.s, name="s"),
    path('homes/', views.homes, name="homes"),
    path('skills/', views.skills, name="skills"),
    path('experiences/', views.experiences, name="experiences"),
    path('achievements/', views.achievements, name="achievements"),
    path('matkul/', views.matkul_create, name = "matkul"),
    path('jadwal/', views.jadwalmatkul, name = "jadwal"),
    path('delete/<delete_subject>', views.delete_matkul, name="delete"),
    path('details/<detail_subject>', views.detail, name="details"),
]
