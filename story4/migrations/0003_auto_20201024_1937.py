# Generated by Django 3.1.1 on 2020-10-24 12:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4', '0002_matkul_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkul',
            name='credits',
            field=models.CharField(choices=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('6', '6')], default='1', max_length=120),
        ),
        migrations.AlterField(
            model_name='matkul',
            name='description',
            field=models.TextField(default='', max_length=500),
        ),
        migrations.AlterField(
            model_name='matkul',
            name='term',
            field=models.CharField(choices=[('2019/2020-1', '2019/2020-1'), ('2019/2020-2', '2019/2020-2'), ('2020/2021-3', '2020/2021-3'), ('2020/2021-4', '2020/2021-4'), ('2021/2022-5', '2021/2022-5'), ('2021/2022-6', '2021/2022-6'), ('2022/2023-7', '2022/2023-7'), ('2022/2023-8', '2022/2023-8')], default='2020/2021-3', max_length=120),
        ),
    ]
