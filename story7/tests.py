from django.test import TestCase, Client
from django.urls import resolve
from .views import accordion

# Create your tests here.

class Testing(TestCase):
    def test_apakah_template_ada(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response,'story7/accordion.html')

    def test_apakah_ada_tulisan_diAccordion(self):
        response = Client().get('/accordion/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Accordion",html_kembalian)

    def test_url_is_exist_accordion_get_failed(self):
        response = Client().get('/accordionn/')
        self.assertEqual(response.status_code, 404)

    def test_url_is_exist_accordion(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)
    
    def test_view_accordion_page_is_using_function_accordion(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, accordion)