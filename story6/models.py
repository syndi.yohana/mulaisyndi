from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    aktivitas = models.CharField(max_length = 120)

    def __str__(self):
        return self.aktivitas

class Peserta(models.Model):
    nama       = models.CharField(max_length = 120)
    kegiatan   = models.ForeignKey(Kegiatan,default=None, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nama