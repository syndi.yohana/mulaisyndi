from django.shortcuts import render, redirect
from .forms import KegiatanForm,PesertaForm
from .models import Kegiatan,Peserta
# Create your views here.
def kegiatan(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context = {
        'kegiatan' : kegiatan,
        'peserta' : peserta
    }
    return render(request, 'story6/kegiatan.html', context)

def formkegiatan(request):
    form_kegiatan = KegiatanForm()
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        form.save()
        return redirect('story6:kegiatan')
    context = {
        'form_kegiatan' : form_kegiatan
    }
    return render(request, 'story6/formkegiatan.html',context)

def formpeserta(request, namakegiatan):
    form_peserta = PesertaForm()
    if request.method == 'POST':
        form = PesertaForm(request.POST)
        form.save()
 
    context = {
        'form_peserta' : form_peserta
    }
    return render(request, 'story6/formpeserta.html',context)