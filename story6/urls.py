from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('',views.kegiatan, name='kegiatan'),
    path('kegiatan/',views.kegiatan, name='kegiatan'),
    path('formpeserta/<namakegiatan>', views.formpeserta, name="formpeserta"),
    path('formkegiatan/', views.formkegiatan, name="formkegiatan"),
]