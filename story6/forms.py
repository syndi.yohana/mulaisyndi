from django import forms
from .models import Kegiatan, Peserta
from django.forms import ModelForm

class KegiatanForm(forms.ModelForm):
    class Meta :
        model = Kegiatan
        fields = [
            'aktivitas',  
        ]

        widgets = {
            'aktivitas':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your  new activitie'}),
        }

class PesertaForm(forms.ModelForm):
    class Meta :
        model = Peserta
        fields = [
            'nama',
            'kegiatan',
        ]

        widgets = {
            'nama':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your  name'}),
        }