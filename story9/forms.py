from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

        widgets = {
            # 'username':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your username'}),
            # 'email':forms.TextInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
            'password1':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your password'}),
            'password2':forms.TextInput(attrs={'class':'form-control','placeholder':'Confirm your password'})
            }