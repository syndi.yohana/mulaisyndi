from django.shortcuts import render, redirect, resolve_url 
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import RegisterForm
# Create your views here.

def register(request):
    if (request.user.is_authenticated):
        return redirect('story4:homes')
    else:
        form = RegisterForm()
        if request.method == 'POST':
            form = RegisterForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)
                return redirect('story9:login')
    context = {'form':form}
    return render(request, 'story9/register.html', context)

def login_page(request):
    if request.user.is_authenticated:
        return redirect('story4:homes')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password =request.POST.get('password')
            
            user = authenticate(request, username=username, password=password)
            
            if user is not None:
                if user.is_active:
                    request.session.set_expiry(3000)
                    login(request, user)
                    return redirect('story4:homes')
            else:
                messages.info(request, 'Username OR password is incorrect')
    context = {}
    return render(request, 'story9/login.html', context)

@login_required
def logout_page(request):
    logout(request)
    return redirect('story9:login')