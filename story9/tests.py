from django.test import TestCase, Client
from django.urls import resolve
from .views import login_page, register, logout_page
from django.contrib.auth.models import User

# Create your tests here.
class Testing(TestCase):
    #template
    def test_apakah_template_register_ada(self):
        response = Client().get('/authuser/register/')
        self.assertTemplateUsed(response,'story9/register.html')
    
    def test_apakah_template_login_ada(self):
        response = Client().get('/authuser/login/')
        self.assertTemplateUsed(response,'story9/login.html')

    def test_apakah_ada_tulisan_diregister(self):
        response = Client().get('/authuser/register/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Register",html_kembalian)

    def test_apakah_ada_tulisan_dilogin(self):
        response = Client().get('/authuser/login/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Login",html_kembalian)
        
    #url
    def test_url_is_exist_login_get_failed(self):
        response = Client().get('/authuser/loginn/')
        self.assertEqual(response.status_code, 404)

    def test_url_is_exist_register_get_failed(self):
        response = Client().get('/authuser/registerr/')
        self.assertEqual(response.status_code, 404)

    def test_url_is_exist_register(self):
        response = Client().get('/authuser/register/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_login(self):
        response = Client().get('/authuser/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_exist_logout(self):
        response = Client().get('/authuser/logout/')
        self.assertEqual(response.status_code, 302)
        
    #view
    def test_view_register_page_is_using_function_resgiter(self):
        found = resolve('/authuser/register/')
        self.assertEqual(found.func, register)

    def test_view_login_page_is_using_function_login(self):
        found = resolve('/authuser/login/')
        self.assertEqual(found.func, login_page)

    def test_view_logout_page_is_using_function_logout(self):
        found = resolve('/authuser/logout/')
        self.assertEqual(found.func, logout_page)

    #user
    def test_user_added(self):
        User.objects.create(username="syndi", email="syndi@gmail.com")
        test = User.objects.all().count()
        self.assertEquals(test, 1)
        nama=User.objects.get(username="syndi")
        self.assertEquals("syndi",str(nama))