"""mulaisyndi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from story6.views import formkegiatan
from story6.views import formpeserta
from story8.views import dataBuku

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('home.urls')),
    path('', include('story4.urls')),
    path('kegiatan/',include('story6.urls')),
    path('formkegiatan/', formkegiatan),
    path('formpeserta/<namakegiatan>', formpeserta),
    path('accordion/',include('story7.urls')),
    path('searchbook/',include('story8.urls')),
    path('dataBuku/', dataBuku),
    path('authuser/', include('story9.urls', namespace = 'authuser')),
]