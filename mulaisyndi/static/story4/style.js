$(document).ready(function(){
    $(".accordion_header").click(function(){
        $(".accordion_header").removeClass("active")
        $(this).addClass("active");
    })
});

$(".reorder-up").click(function(){
  var $current = $(this).closest('.wrapper')
  var $previous = $current.prev('.wrapper');
  if($previous.length !== 0){
    $current.insertBefore($previous);
  }
return false;
});

$(".reorder-down").click(function(){
  var $current = $(this).closest('.wrapper')
  var $next = $current.next('.wrapper');
  if($next.length !== 0){
    $current.insertAfter($next);
  }
return false;
});