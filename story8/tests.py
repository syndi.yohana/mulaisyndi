from django.test import TestCase, Client
from django.urls import resolve
from .views import searchbook, dataBuku

# Create your tests here.
class Testing(TestCase):
    def test_apakah_template_ada(self):
        response = Client().get('/searchbook/')
        self.assertTemplateUsed(response,'story8/searchbook.html')

    def test_apakah_ada_tulisan_disearchbook(self):
        response = Client().get('/searchbook/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Search Books!",html_kembalian)

    def test_url_is_exist_searchbook_get_failed(self):
        response = Client().get('/Searchbooks/')
        self.assertEqual(response.status_code, 404)

    def test_url_is_exist_searchbook(self):
        response = Client().get('/searchbook/')
        self.assertEqual(response.status_code, 200)
    
    def test_view_searchbook_page_is_using_function_searchbook(self):
        found = resolve('/searchbook/')
        self.assertEqual(found.func, searchbook)

    def test_view_dataBuku_page_is_using_function_dataBuku(self):
        found = resolve('/dataBuku/')
        self.assertEqual(found.func, dataBuku)

    def test_pemanggilan_tautan(self):
        response = Client().get('/dataBuku/?q=harry%202')
        self.assertEqual(response.status_code, 200)