from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def searchbook(request):
    return render(request, 'story8/searchbook.html')

def dataBuku(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    hasil = requests.get(url)
    data = json.loads(hasil.content)
    if (not request.user.is_authenticated):
        i=0
        for x in data['items']:
            if (data['items'][i]['volumeInfo']['imageLinks'] != ''):
                del data['items'][i]['volumeInfo']['imageLinks']['thumbnail']
            del data['items'][i]['volumeInfo']['infoLink']
            i=i+1
    return JsonResponse(data, safe=False)